===============================
neutron-interconnection
===============================

This project implements a Neutron API extension to provide network connectivity between two or more Openstack deployments or regions.

Please fill here a long description which must be at least 3 lines wrapped on
80 cols, so that distribution package maintainers can use it in their packages.
Note that this is a hard requirement.

* Free software: Apache license
* Documentation: https://opendev.org/x/neutron-interconnection/src/branch/master/doc/source
* Source: https://opendev.org/x/neutron-interconnection
* Bugs: https://bugs.launchpad.net/neutron-interconnection

--------

* TODO
