===================
Configuration Guide
===================

Policy
------

Like most OpenStack projects, neutron-interconnection uses policies to restrict
permissions on REST API actions.

.. toctree::
   :maxdepth: 1

   Policy Reference <policy>
   Sample Policy File <policy-sample>
