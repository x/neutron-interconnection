==========================================
Neutron Interconnection Sample Policy File
==========================================

The following is a neutron-interconnection sample policy file for adaptation
and use.

This sample policy can also be viewed in :download:`file form
</_static/neutron-interconnection.policy.yaml.sample>`.

.. important::

   The sample policy file was auto-generated when neutron-interconnection
   documentation was build. You must ensure your neutron-interconnection
   version matches the version of this documentation.

.. literalinclude:: /_static/neutron-interconnection.policy.yaml.sample
